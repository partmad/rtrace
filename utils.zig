pub const std = @import("std");

const c = @cImport({
    @cInclude("SDL.h");
    @cInclude("SDL_ttf.h");
    @cInclude("SDL_opengl.h");
});

const panic = std.debug.panic;

pub const Allocator = std.mem.Allocator;
pub const ArrayList = std.ArrayList;
pub const assert = std.debug.assert;

pub const Rect = struct {
    x: i32,
    y: i32,
    w: i32,
    h: i32,
};

pub const Color = packed struct {
    r: u8,
    g: u8,
    b: u8,
    a: u8,

    pub fn toFloat01(self: Color) ColorFloat {
        return ColorFloat {
            .r = @intToFloat(f32, self.r) / 255.0,
            .g = @intToFloat(f32, self.g) / 255.0,
            .b = @intToFloat(f32, self.b) / 255.0,
            .a = @intToFloat(f32, self.a) / 255.0,
        };
    }
};

pub const LightGray = Color { .r = 200, .g = 200, .b = 200, .a = 255 };
pub const DarkGray = Color { .r = 40, .g = 40, .b = 40, .a = 255 };

pub const ColorFloat = packed struct {
    r: f32,
    g: f32,
    b: f32,
    a: f32,
};

pub const Vec2f = packed struct {
    x: f32,
    y: f32,

    pub fn toInt(self: Vec2f) Vec2i {
        return Vec2i {
            .x = @floatToInt(i32, self.x),
            .y = @floatToInt(i32, self.y),
        };
    }
};

pub fn vec2f(x: f32, y: f32) Vec2f {
    return Vec2f {
        .x = x,
        .y = y,
    };
}

pub fn div(self: Vec2f, other: Vec2f) Vec2f {
    return Vec2f {
        .x = self.x / other.x,
        .y = self.y / other.y,
    };
}

pub fn mul(self: Vec2f, other: Vec2f) Vec2f {
    return Vec2f {
        .x = self.x * other.x,
        .y = self.y * other.y,
    };
}

pub const Vec2i = packed struct {
    x: i32,
    y: i32,

    pub fn toFloat(self: Vec2i) Vec2f {
        return Vec2f {
            .x = @intToFloat(f32, self.x),
            .y = @intToFloat(f32, self.y),
        };
    }
};

pub fn vec2i(x: i32, y: i32) Vec2i {
    return Vec2i {
        .x = x,
        .y = y,
    };
}

pub fn max(a: Vec2i, b: Vec2i) Vec2i {
    return Vec2i {
        .x = std.math.max(a.x, b.x),
        .y = std.math.max(a.y, b.y),
    };
}

pub fn min(a: Vec2i, b: Vec2i) Vec2i {
    return Vec2i {
        .x = std.math.min(a.x, b.x),
        .y = std.math.min(a.y, b.y),
    };
}

pub fn add(a: Vec2i, b: Vec2i) Vec2i {
    return Vec2i {
        .x = a.x + b.x,
        .y = a.y + b.y,
    };
}

pub fn sub(a: Vec2i, b: Vec2i) Vec2i {
    return Vec2i {
        .x = a.x - b.x,
        .y = a.y - b.y,
    };
}

pub fn Tri(comptime t: type) type {
    // TODO which direction?
    return packed struct {
        a: t,
        b: t,
        c: t,
    };
}

pub fn Quad(comptime t: type) type {
    return packed struct {
        tl: t,
        tr: t,
        bl: t,
        br: t,
    };
}


pub fn oom() noreturn {
    std.debug.panic("oom", .{});
}