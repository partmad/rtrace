const utils = @import("utils.zig");
usingnamespace utils;

const c = @cImport({
    @cInclude("SDL.h");
    @cInclude("SDL_ttf.h");
    @cInclude("SDL_opengl.h");
});

const panic = std.debug.panic;

const fonts = @import("font.zig");


pub fn main() !void {
    if (c.SDL_Init(c.SDL_INIT_VIDEO) != 0) {
        c.SDL_Log("Unable to initialize SDL: %s", c.SDL_GetError());
        return error.SDLInitializationFailed;
    }
    defer c.SDL_Quit();

    // var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    // const allocator = &gpa.allocator;
    // defer {
    //     const leaked = gpa.deinit();
    //     if (leaked) @panic("memory leak\n");
    // }
    const allocator = std.heap.page_allocator;
    var arena = std.heap.ArenaAllocator.init(allocator);

    var filecontents = try FileContents.openFile(&arena.allocator, "C:\\Users\\c803330\\Projects\\zig\\zdbgui\\build.zig");
    var file_topline: i32 = 0;

    var font = fonts.Font.default(allocator, 16, LightGray, DarkGray) catch panic("cannot initialize default font", .{});

    var window = try Window.init(&arena.allocator, 800, 600);
    defer window.deinit();

    var events = ArrayList(c.SDL_Event).init(allocator);
    var quit = false;
    var debug = false;


    while (!quit) {
        try events.resize(0);
        var event: c.SDL_Event = undefined;

        while (c.SDL_PollEvent(&event) != 0) {
            switch (event.type) {
                c.SDL_QUIT => quit = true,

                c.SDL_MOUSEWHEEL => file_topline = file_topline - event.wheel.y,

                c.SDL_KEYDOWN => {
                    switch (event.key.keysym.sym) {
                        c.SDLK_ESCAPE => quit = true,
                        c.SDLK_z => debug = true,
                        //else => { std.debug.print("keycode: {any}\n", .{event.key}); },
                        else => {},
                    }
                },

                else => {},
            }
        }

        window.render(&filecontents, file_topline, &font, debug);

        debug = false;
        c.SDL_Delay(17);
    }
}

pub const FileContents = struct {
    filename: []const u8,
    contents: ArrayList(u8),
    line_ranges: ArrayList([2]usize),

    pub fn openFile(allocator: *Allocator, filename: []const u8) !FileContents {
        const file = try std.fs.cwd().openFile(filename, .{});
        defer file.close();

        const stat = try file.stat();
        const filesize = stat.size;

        const bytes = allocator.alloc(u8, filesize) catch oom();
        const len = try file.readAll(bytes);

        var contents = ArrayList(u8).init(allocator);
        contents.appendSlice(bytes) catch oom();

        var lines = ArrayList([2]usize).init(allocator);
        var start: usize = 0;
        while (start <= len) {
            var end = start;
            while (end < len and bytes[end] != '\n') { end += 1; }
            lines.append(.{start, end}) catch oom();
            start = end + 1;
        }

        return FileContents {
            .filename = filename,
            .contents = contents,
            .line_ranges = lines,
        };
    }
};

const Window = struct {
    sdl_window: *c.SDL_Window,
    width: i32,
    height: i32,

    allocator: *Allocator,

    gl_context: c.SDL_GLContext,
    texture_buffer: ArrayList(Quad(Vec2f)),
    vertex_buffer: ArrayList(Quad(Vec2f)),
    color_buffer: ArrayList(Quad(Color)),
    index_buffer: ArrayList([2]Tri(u32)),

    pub fn init(allocator: *Allocator, width: i32, height: i32) !Window {
        // TODO: pass window name as parameter
        // Create window
        const window_flags = c.SDL_WINDOW_OPENGL | c.SDL_WINDOW_ALLOW_HIGHDPI | c.SDL_WINDOW_RESIZABLE;
        const sdl_window = c.SDL_CreateWindow(
            "ZigDB",
            c.SDL_WINDOWPOS_UNDEFINED, c.SDL_WINDOWPOS_UNDEFINED,
            @as(c_int, width), @as(c_int, height),
            @intCast(u32, window_flags)
        ) orelse { return error.NoGLContext; };

        // init opengl
        const gl_context = c.SDL_GL_CreateContext(sdl_window);
        if (c.SDL_GL_MakeCurrent(sdl_window, gl_context) != 0)
            return error.NoGLContext;
        
        // TODO
        c.glEnable(c.GL_BLEND);
        c.glBlendFunc(c.GL_SRC_ALPHA, c.GL_ONE_MINUS_SRC_ALPHA);
        c.glDisable(c.GL_CULL_FACE);
        c.glDisable(c.GL_DEPTH_TEST);
        c.glEnable(c.GL_TEXTURE_2D);
        c.glEnableClientState(c.GL_VERTEX_ARRAY);
        c.glEnableClientState(c.GL_TEXTURE_COORD_ARRAY);
        c.glEnableClientState(c.GL_COLOR_ARRAY);

        const font = fonts.Font.default(allocator, 16, LightGray, DarkGray) catch panic("cannot initialize default font", .{});
        loadFontIntoGLTexture(font);

        if (c.SDL_GL_SetSwapInterval(0) != 0)
            return error.NoVSYNC;
        
        //c.SDL_StartTextInput();

        return Window {
            .sdl_window = sdl_window,
            .width = width,
            .height = height,

            .allocator = allocator,

            .gl_context = gl_context,
            .texture_buffer = ArrayList(Quad(Vec2f)).init(allocator),
            .vertex_buffer = ArrayList(Quad(Vec2f)).init(allocator),
            .color_buffer = ArrayList(Quad(Color)).init(allocator),
            .index_buffer = ArrayList([2]Tri(u32)).init(allocator),
        };
    }

    pub fn deinit(self: *Window) void {
        self.index_buffer.deinit();
        self.color_buffer.deinit();
        self.vertex_buffer.deinit();
        self.texture_buffer.deinit();
        
        c.SDL_GL_DeleteContext(self.gl_context);
        c.SDL_DestroyWindow(self.sdl_window);
    }

    pub fn render(self: *Window, file: *FileContents, topline: i32, font: *fonts.Font, debug_print: bool) void {
        c.SDL_GL_GetDrawableSize(self.sdl_window, &self.width, &self.height);
        const window_rect = Rect { .x = 0, .y = 0, .w = self.width, .h = self.height };

        const topdisplayline = @intCast(usize, std.math.clamp(topline, 0, file.line_ranges.items.len));
        const el = topline + @divTrunc(self.height, font.line_advance);
        const enddisplayline = @intCast(usize, std.math.clamp(el, 0, file.line_ranges.items.len));
        
        var line_y = window_rect.y;
        if (topline < 0) {
            line_y -= topline * font.line_advance;
        }

        for (file.line_ranges.items[topdisplayline..enddisplayline]) |line_range, line_index| {
            const line = file.contents.items[line_range[0]..line_range[1]];
            const line_rect = Rect {
                .x = window_rect.x,
                .y = line_y,
                .w = window_rect.w,
                .h = window_rect.h,
            };
            self.queueText(line_rect, font, line);

            line_y += font.line_advance;

            if (debug_print) {
                std.debug.print("line_index: {any}; line_rect.y: {any}; line: {s}\n", .{line_index, line_rect.y, line});
                //std.debug.print("topdisplayline: {any}; enddisplayline: {any}; line y: {any}; line: {s}\n", .{topdisplayline, enddisplayline, line_rect.y, line});
            }
        }

        // render warnings
        // const num_warnings = self.warnings.items.len;

        // display window
        if (c.SDL_GL_MakeCurrent(self.sdl_window, self.gl_context) != 0)
            panic("SDL_GL_MakeCurrent", .{});

        const bg_color = font.background_color.toFloat01();
        c.glClearColor(bg_color.r, bg_color.g, bg_color.b, bg_color.a);
        c.glClear(c.GL_COLOR_BUFFER_BIT);

        c.glViewport(0, 0, self.width, self.height);
        c.glMatrixMode(c.GL_PROJECTION);
        c.glPushMatrix();
        c.glLoadIdentity();
        c.glOrtho(0.0, @intToFloat(f32, self.width), @intToFloat(f32, self.height), 0.0, -1.0, 1.0);
        c.glMatrixMode(c.GL_MODELVIEW);
        c.glPushMatrix();
        c.glLoadIdentity();

        c.glTexCoordPointer(2, c.GL_FLOAT, 0, self.texture_buffer.items.ptr);
        c.glVertexPointer(2, c.GL_FLOAT, 0, self.vertex_buffer.items.ptr);
        c.glColorPointer(4, c.GL_UNSIGNED_BYTE, 0, self.color_buffer.items.ptr);
        c.glDrawElements(c.GL_TRIANGLES, @intCast(c_int, self.index_buffer.items.len) * 6, c.GL_UNSIGNED_INT, self.index_buffer.items.ptr);

        c.glMatrixMode(c.GL_MODELVIEW);
        c.glPopMatrix();
        c.glMatrixMode(c.GL_PROJECTION);
        c.glPopMatrix();

        c.SDL_GL_SwapWindow(self.sdl_window);

        // reset buffers
        self.texture_buffer.resize(0) catch oom();
        self.vertex_buffer.resize(0) catch oom();
        self.color_buffer.resize(0) catch oom();
        self.index_buffer.resize(0) catch oom();
    }

    pub fn queueQuad(self: *Window, dst_pos: Vec2f, dst_size: Vec2f, src_pos: Vec2f, src_size: Vec2f,  texture_size: Vec2f, color: Color) void {
        const tp = div(src_pos, texture_size);
        const ts = div(src_size, texture_size);
        self.texture_buffer.append(.{
            .tl = .{ .x = tp.x,         .y = tp.y },
            .tr = .{ .x = tp.x + ts.x,  .y = tp.y },
            .bl = .{ .x = tp.x,         .y = tp.y + ts.y },
            .br = .{ .x = tp.x + ts.x,  .y = tp.y + ts.y },
        }) catch oom();

        self.vertex_buffer.append(.{
            .tl = .{ .x = dst_pos.x,                .y = dst_pos.y },
            .tr = .{ .x = dst_pos.x + dst_size.x,   .y = dst_pos.y },
            .bl = .{ .x = dst_pos.x,                .y = dst_pos.y + dst_size.y },
            .br = .{ .x = dst_pos.x + dst_size.x,   .y = dst_pos.y + dst_size.y },
        }) catch oom();

        self.color_buffer.append(.{
            .tl = color,
            .tr = color,
            .bl = color,
            .br = color,
        }) catch oom();

        const vertex_ix = @intCast(u32, self.index_buffer.items.len * 4);
        self.index_buffer.append(.{
            .{
                .a = vertex_ix + 0,
                .b = vertex_ix + 1,
                .c = vertex_ix + 2,
            },
            .{
                .a = vertex_ix + 2,
                .b = vertex_ix + 3,
                .c = vertex_ix + 1,
            },
        }) catch oom();
    }

    pub fn queueText(self: *Window, rect: Rect, font: *fonts.Font, chars: []const u8) void {
        const max_pos = vec2i(rect.x + rect.w, rect.y + rect.h);
        var dst_pos = vec2i(rect.x, rect.y);

        for (chars) |char| {
            const char_size = font.char_size(char);
            const src = font.char_pos(char);
            
            const max_size = sub(max_pos, dst_pos);
            const ratio = div(min(max_size, char_size).toFloat(), char_size.toFloat());

            const dst_size = char_size.toFloat();
            const src_size = mul(dst_size, ratio);
            const tsize = vec2i(font.texture_width, font.texture_height).toFloat();

            self.queueQuad(dst_pos.toFloat(), dst_size, src, src_size, tsize, font.text_color);
            
            dst_pos.x += char_size.x;
        }
    }
};


pub fn loadFontIntoGLTexture(font: fonts.Font) void {
    var id: u32 = undefined;
    c.glGenTextures(1, &id);
    c.glBindTexture(c.GL_TEXTURE_2D, id);
    c.glTexImage2D(c.GL_TEXTURE_2D, 0, c.GL_ALPHA, font.texture_width, font.texture_height, 0, c.GL_RGBA, c.GL_UNSIGNED_BYTE, font.texture.ptr);
    c.glTexParameteri(c.GL_TEXTURE_2D, c.GL_TEXTURE_MIN_FILTER, c.GL_NEAREST);
    c.glTexParameteri(c.GL_TEXTURE_2D, c.GL_TEXTURE_MAG_FILTER, c.GL_NEAREST);
}