const utils = @import("utils.zig");
usingnamespace utils;

const c = @cImport({
    @cInclude("SDL.h");
    @cInclude("SDL_ttf.h");
    @cInclude("SDL_opengl.h");
});

const panic = std.debug.panic;
// const Allocator = std.mem.Allocator;
const fira_code = @embedFile("FiraCode-Regular.woff");

pub const Font = struct {
    ttf_font: *c.TTF_Font,
    font_size: usize,
    texture: []Color,
    texture_width: i32,
    texture_height: i32,
    char_width: i32,
    char_height: i32,
    line_advance: i32,
    char_position: []Vec2f,
    white_rect_pos: Vec2f,

    text_color: Color,
    background_color: Color,

    pub fn default(alloc: *Allocator, font_size: u32, text_color: Color, bg_color: Color) !Font {
        // Init SDL_TTF
        if(c.TTF_Init() != 0)
            panic("Cannot init TTF_Init", .{});
        defer c.TTF_Quit();

        // Load font
        // var reader = c.SDL_RWFromConstMem(fira_code, @intCast(c_int, fira_code.len)) orelse {
        //     panic("cannot load default font", .{});
        // };
        // const font = c.TTF_OpenFontRW(reader, 1, @intCast(c_int, font_size)) orelse {
        //     panic("cannot read default font", .{});
        // };

        const font = c.TTF_OpenFont(".\\include\\FiraCode-Regular.ttf", @intCast(c_int, font_size)) orelse {
            panic("cannot read default font", .{});
        };

        // Create text array with all ascii characters
        var text = try alloc.allocSentinel(u8, 128, 0);
        defer alloc.free(text);
        text[0] = ' ';
        {
            var char: usize = 1;
            while (char < text.len) : (char += 1) {
                text[char] = @intCast(u8, char);
            }
        }

        // Render all characters to a surface
        const surface = c.TTF_RenderUTF8_Blended(font, text, c.SDL_Color{ .r = 255, .g = 255, .b = 255, .a = 255 });
        defer c.SDL_FreeSurface(surface);

        // copy texture
        const texture = try std.mem.dupe(alloc, Color, @ptrCast([*]Color, surface.*.pixels)[0..@intCast(usize, surface.*.w * surface.*.h)]);

        // make a white pixel -- ?
        // TODO: use same width/height as characters
        texture[0] = Color{ .r = 255, .g = 255, .b = 255, .a = 255 };
        const white_rect_pos = Vec2f{ .x = 0.0, .y = 0.0, };

        // char sizes for monospaced font
        const char_width = @intCast(i32, @divTrunc(@intCast(usize, surface.*.w), text.len));
        const char_height = @intCast(i32, surface.*.h);

        // calculate location of each char
        var char_position = try alloc.alloc(Vec2f, text.len);
        char_position[0] = .{ .x = 0.9, .y = 0.0 };
        {
            var char: usize = 1;
            while (char < text.len ) : ( char += 1 ) {
                char_position[char] = .{
                    .x = @intToFloat(f32, char) * @intToFloat(f32, char_width),
                    .y = 0.0,
                };
            }
        }

        return Font {
            .ttf_font = font,
            .font_size = font_size,
            .texture = texture,
            .texture_width = @intCast(i32, surface.*.w),
            .texture_height = @intCast(i32, surface.*.h),
            .char_width = char_width,
            .char_height = char_height,
            .line_advance = char_height,
            .char_position = char_position,
            .white_rect_pos = white_rect_pos,

            .text_color = text_color,
            .background_color = bg_color,
        };
    }

    pub fn char_size(self: *Font, _: u8) Vec2i {
        return Vec2i {
            .x = self.char_width, 
            .y = self.char_height
        };
    }

    pub fn char_pos(self: *Font, char: u8) Vec2f {
        const result = if (char < self.char_position.len) 
            self.char_position[char]
        else
            self.white_rect_pos;
        
        return result;
    }
};


